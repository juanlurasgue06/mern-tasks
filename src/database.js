const mongoose = require("mongoose");

const URI = 'mongodb://localhost/mern-tasks';

mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(db => console.log("La base de datos se ha conectado."))
    .catch(err => console.log(err));

module.export = mongoose;