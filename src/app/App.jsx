import React, { Component } from 'react';

class App extends Component {

    constructor(){
        super();
        this.state = {
            title: '', 
            description:'',
            tasks: [],
            _id: ''
        };
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    addTask(e){
        if(this.state._id){
            fetch(`api/tasks/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res=> res.json())
            .then(data => {
                M.toast({html: 'Tarea editada'});
                this.setState({title:'', description:'', _id:''});
                this.getTasks(); 
            })
            .catch(err => console.log(err));
            e.preventDefault();

        } else{
            fetch('api/tasks', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res=> res.json())
            .then(data => {
                M.toast({html: 'Tarea guardada'});
                this.setState({title:'', description:''});
                this.getTasks(); 
            })
            .catch(err => console.log(err));
            e.preventDefault();
        }
    }

    componentDidMount(){
        this.getTasks()
    }

    getTasks(){
        fetch('api/tasks').then(res=> res.json())
        .then(data => {
            this.setState({tasks: data})
        })
        .catch(err => console.log(err));
    }

    editTask(id){
        fetch(`api/tasks/${id}`).then(res=> res.json())
        .then(data => {
            this.setState({title:data.title, description:data.description, _id:data._id})
        })
        .catch(err => console.log(err));
    }

    deleteTask(id){
        if(confirm('¿Estás seguro de querer eliminar el elemento?')){
            fetch(`api/tasks/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res=> res.json())
            .then(data => {
                M.toast({html: 'Tarea borrada'});
                this.getTasks(); 
            })
            .catch(err => console.log(err));
        }
    }

    handleChange(e){
        const {name, value} = e.target;
        this.setState({
            [name]: value
        })
        e.preventDefault();
    }

    render() {
        return ( 
            <div>
                {/*Navigation*/}
                <nav className = "red lighten-2">
                    <div className="container">
                        <a className="brand-logo" href="/"> MERN Task</a>
                        
                    </div>
                </nav>
                <div className="container">
                    <div className="row">
                        <div className="col s5">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addTask}>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input name="title" onChange={this.handleChange} type="text" placeholder="Título" value={this.state.title}/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <textarea name="description" onChange={this.handleChange}  className="materialize-textarea" placeholder="Descripción" value={this.state.description}></textarea>
                                            </div>
                                        </div>
                                        <button className="btn red lighten-2">Enviar</button>
                                    </form>
                                    
                                </div>
                            </div>
                            
                        </div>
                        <div className="col s7" >
                            <table>
                                <thead>
                                    <tr>
                                        <th>Título</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tasks.map(task =>{
                                            return(
                                                <tr key={task._id}>
                                                    <td>{task.title}</td>
                                                    <td>{task.description}</td>
                                                    <td>
                                                        <button className="btn red lighten-2" onClick={()=> this.editTask(task._id)}>
                                                            <i className="material-icons">edit</i>
                                                        </button>
                                                        <button className="btn red lighten-2" style={{margin:"4px"}}  onClick={()=> this.deleteTask(task._id)}>
                                                            <i className="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default App;

