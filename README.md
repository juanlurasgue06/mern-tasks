## CRUD. React, Node, Express and Mongodb
Source code of a CRUD application example with React, Node, Express and Mongodb

```
npm install
```

### Run

```
npm run dev
```

### Webpack compile

```
npm run webpack
```
